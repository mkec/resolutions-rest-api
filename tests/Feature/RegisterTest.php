<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    public function testsRegistersSuccessfully()
    {
        //prepare data for registration
        $user_data = [
            'name' => 'John',
            'email' => 'john@doe.com',
            'password' => 'password',
            'password_confirmation' => 'password',
        ];
        //try to register and check if new resource is created with specific structure
        $res = $this->json('post', '/api/register', $user_data)
            ->assertStatus(201)
            ->assertJsonStructure(['name', 'email', 'api_token']);
    }

    public function testsRequiresPasswordEmailAndName()
    {
        //try to register without data
        $this->json('post', '/api/register')
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'name' => ['The name field is required.'],
                    'email' => ['The email field is required.'],
                    'password' => ['The password field is required.'],
                ]
            ]);
    }

    public function testsRequirePasswordConfirmation()
    {
        $user_data = [
            'name' => 'John',
            'email' => 'john@doe.com',
            'password' => 'password',
        ];
        //check response if input is ommited
        $this->json('post', '/api/register', $user_data)
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'password' => ['The password confirmation does not match.'],
                ]
            ]);
    }
}
