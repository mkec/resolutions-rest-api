<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class ResolutionControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testCanReturnCollectionOfResources()
    {
        $resolution = $this->create('Resolution');
        $resolution2 = $this->create('Resolution');
        $resolution3 = $this->create('Resolution');

        $response = $this->actingAs($this->user(), 'api')
            ->json('GET', '/api/resolutions');
        $response
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id', 'title', 'body', 'reminder', 'created_at'
                    ]

                ],
                'links' => ['first', 'last', 'prev', 'next'],
                'meta' => [
                    'current_page', 'last_page', 'from', 'to', 'path', 'per_page', 'total'
                ]
            ])
            ->assertStatus(200);
    }

    public function testCanReturnCollectionOfPublishedResources()
    {
        $resolution = $this->create('Resolution');
        $resolution2 = $this->create('Resolution');
        $resolution3 = $this->create('Resolution');

        $response = $this->actingAs($this->user(), 'api')
            ->json('GET', '/api/resolutions/published');
        $response
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id', 'title', 'body', 'reminder', 'created_at'
                    ]

                ],
                'links' => ['first', 'last', 'prev', 'next'],
                'meta' => [
                    'current_page', 'last_page', 'from', 'to', 'path', 'per_page', 'total'
                ]
            ])
            ->assertStatus(200);
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCanCreateResolution()
    {
        $faker = Factory::create();
        //Given
            // user is authenticated
        //When
            // post request create product
        $content = "[{\"type\":\"paragraph\",\"children\":[{\"text\":\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a \"},{\"text\":\"galley\",\"bold\":true},{\"text\":\" of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining \"},{\"text\":\"essentially\",\"underline\":true,\"bold\":true},{\"text\":\" unchanged. It was popularised in the \"},{\"text\":\"1960s\",\"code\":true},{\"text\":\" with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like \"},{\"text\":\"Aldus PageMaker\",\"underline\":true},{\"text\":\" including versions of \"},{\"text\":\"Lorem Ipsum\",\"italic\":true},{\"text\":\".\"}]}]";
        $response = $this->actingAs($this->user(), 'api')->json('POST', '/api/resolutions', [
            'title' => $title = $faker->name,
            'content' => $content,
            'reminder' => $reminder = random_int(0, 99),
            'user_id' => $this->user()->id
        ]);
        //Log::info($response->getContent());
        //Then
            // product exists
        //dump($response);
        $response
            ->assertJsonStructure([
                'id', 'title', 'body', 'reminder', 'created_at'
            ])
            ->assertJson([
                'title' => $title,
                'body' => $content,
                'reminder' => $reminder
            ])
            ->assertStatus(201);
        $this->assertDatabaseHas('resolutions', [
            'title' => $title,
            'content' => $content,
            'reminder' => $reminder
        ]);
    }

    public function testWillFailWithA404IfResolutionIsNotFound()
    {
        $response = $this->json('GET', 'api/resolutions/-1');
        $response->assertStatus(404);
    }

    public function testCanReturnResolution()
    {
        //Given
        $resolution = $this->create('Resolution');
        //When
        $response = $this->json('GET', "api/resolutions/{$resolution->id}");
        //Then
        $until = $resolution->until? Carbon::parse($resolution->until)->format('d/m/Y') : null;
        $start = $resolution->start? Carbon::parse($resolution->start)->format('d/m/Y') : null;
        $ui_created_at = $resolution->ui_created_at? Carbon::parse($resolution->ui_created_at)->format('d/m/Y') : null;
        $response
            ->assertExactJson([
                'id' => $resolution->id,
                'title' => $resolution->title,
                'body' => $resolution->content,
                'reminder' => (int)$resolution->reminder,
                'created_at' => (string) $resolution->created_at,
                'hash' => md5($resolution->salt . $resolution->id),
                'published' => (int) $resolution->published,
                'ui_created_at' => (string) $ui_created_at,
                'status' => (int) $resolution->status,
                'start' => (string) $start,
                'until' => (string) $until,
                //'updated_at' => (string) $resolution->updated_at
            ])
            ->assertStatus(200);
    }

    public function testWillFailWithA422IfResolutionWeWantToUpdateIsNotFound()
    {
        $response = $this->actingAs($this->user(), 'api')->json('PUT', 'api/resolutions/-1');
        $response->assertStatus(422);
    }

    public function testCanUpdateResolution()
    {
        $resolution = $this->create('Resolution');
        $response = $this->actingAs($this->user(), 'api')->json('PUT', "api/resolutions/$resolution->id", [
            'title' => $resolution->title . "_updated",
            'content' => $resolution->content
        ]);

        $until = $resolution->until? Carbon::parse($resolution->until)->format('d/m/Y') : null;
        $start = $resolution->start? Carbon::parse($resolution->start)->format('d/m/Y') : null;
        $ui_created_at = $resolution->ui_created_at? Carbon::parse($resolution->ui_created_at)->format('d/m/Y') : null;

        $response
            ->assertExactJson([
                'id' => $resolution->id,
                'title' => $resolution->title . "_updated",
                'body' => $resolution->content,
                'reminder' => $resolution->reminder,
                'created_at' => (string) $resolution->created_at,
                'hash' => md5($resolution->salt . $resolution->id),
                'published' => (int) $resolution->published,
                'ui_created_at' => (string) $ui_created_at,
                'status' => (int) $resolution->status,
                'start' => (string) $start,
                'until' => (string) $until,
            ])
            ->assertStatus(200);

        $this->assertDatabaseHas('resolutions', [
            'id' => $resolution->id,
            'title' => $resolution->title . "_updated",
            'content' => $resolution->content ,
            'reminder' => $resolution->reminder,
            'created_at' => (string) $resolution->created_at,
            'updated_at' => (string) $resolution->updated_at
        ]);
    }

    public function testWillFailWithA404IfResolutionWeWantToDeleteIsNotFound()
    {
        $response = $this->actingAs($this->user(), 'api')->json('DELETE', 'api/resolutions/-1');
        $response->assertStatus(404);
    }

    public function testCanDeleteResolution()
    {
        $resolution = $this->create('Resolution');
        $response = $this->actingAs($this->user(), 'api')->json('DELETE', "api/resolutions/$resolution->id");
        $response->assertStatus(204)
            ->assertSee(null);
        $this->assertDatabaseMissing('resolutions', [
            'id' => $resolution->id
        ]);
    }
}
