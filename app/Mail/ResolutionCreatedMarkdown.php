<?php

namespace App\Mail;

use App\Resolution;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ResolutionCreatedMarkdown extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $resolution;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Resolution $resolution)
    {
        $this->resolution = $resolution;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //update resolution field to sent
        //reminder_sent is integer value where we are adding 1 every time when mail is build
        //so we can later upgrade our code
        //it could be just boolean for now
        $this->resolution->reminder_sent = $this->resolution->reminder_sent + 1;
        $this->resolution->save();
        return $this->markdown('emails.resolutions.commented-markdown');
    }
}
