<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreResolution extends FormRequest
{
    /**
     * If validation is failed return HTTP Response Exception 422
     *
     * @param Validator $validator
     * @return HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'bail|min:5|required|max:100|string',
            'content' => 'bail|required|json',
            'reminder' => 'bail|numeric|min:0',
            'start' => 'bail|date_format:"d-m-Y"',
            'until' => 'bail|date_format:"d-m-Y"',
            'status' => 'bail|boolean',
            'published' => 'bail|boolean',
            'ui_created_at' => 'bail|date_format:"d-m-Y"',
        ];
    }
}
