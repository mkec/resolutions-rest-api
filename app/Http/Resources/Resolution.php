<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Resolution extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $until = $this->until? Carbon::parse($this->until)->format('d/m/Y') : null;
        $start = $this->start? Carbon::parse($this->start)->format('d/m/Y') : null;
        $ui_created_at = $this->ui_created_at? Carbon::parse($this->ui_created_at)->format('d/m/Y') : null;
        return [
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->content,
            'reminder' => (int)$this->reminder,
            'ui_created_at' => (string) $ui_created_at,
            'start' => (string) $start,
            'until' => (string) $until,
            'status' => (int) $this->status,
            'published' => (int) $this->published,
            'hash' => md5($this->salt . $this->id),
            'created_at' => (string) $this->created_at
        ];
    }
}
