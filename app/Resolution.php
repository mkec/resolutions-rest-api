<?php

namespace App;

use App\Http\Requests\StoreResolution;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Resolution extends Model
{
    protected $guarded = ['id'];
    /**
     * ORM many to 1 relation
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeLatest(Builder $query)
    {
        return $query->orderBy(static::CREATED_AT, 'desc');
    }

    public function scopeLatestByEmail(Builder $query, $email)
    {
        return $query->latest()->whereHas('user', function (Builder $query) use ($email) {
            $query->where('email', $email);
        });
    }

    public function scopeByPublishedResolutions(Builder $query)
    {
        return $query->latest()->where('published', true);
    }

    public function scopeByExampleResolutions(Builder $query)
    {
        return $query->latest()->whereHas('user', function (Builder $query) {
            $query->where('email', 'milos.kecman@gmail.com');
        });
    }

    public function scopeByHash(Builder $query, $hash)
    {
        return $query->where(DB::raw('md5(concat(`salt`,`id`))'), $hash)->where('published', true);
    }
}
