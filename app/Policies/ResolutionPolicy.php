<?php

namespace App\Policies;

use App\Resolution;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ResolutionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any resolutions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the resolution.
     *
     * @param  \App\User  $user
     * @param  \App\Resolution  $resolution
     * @return mixed
     */
    public function view(User $user, Resolution $resolution)
    {
        return true;
    }

    /**
     * Determine whether the user can create resolutions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the resolution.
     *
     * @param  \App\User  $user
     * @param  \App\Resolution  $resolution
     * @return mixed
     */
    public function update(User $user, Resolution $resolution)
    {
        return $user->id == $resolution->user_id;
    }

    /**
     * Determine whether the user can delete the resolution.
     *
     * @param  \App\User  $user
     * @param  \App\Resolution  $resolution
     * @return mixed
     */
    public function delete(User $user, Resolution $resolution)
    {
        return $user->id == $resolution->user_id;
    }

    /**
     * Determine whether the user can restore the resolution.
     *
     * @param  \App\User  $user
     * @param  \App\Resolution  $resolution
     * @return mixed
     */
    public function restore(User $user, Resolution $resolution)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the resolution.
     *
     * @param  \App\User  $user
     * @param  \App\Resolution  $resolution
     * @return mixed
     */
    public function forceDelete(User $user, Resolution $resolution)
    {
        return false;
    }
}
