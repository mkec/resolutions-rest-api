<?php

namespace App\Console;

use App\Mail\UsersResolutionsReminderMarkdown;
use App\Resolution;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->call(function () {
            //search through notifications table
            //find when notification is sent last time and add reminder days
            $resolutions = new Resolution();
            $forQueue = $resolutions::with('user')->whereDate(
                DB::raw('DATE_ADD(last_sent, INTERVAL reminder DAY)'),
                '=',
                DATE('Y-m-d')
            )->whereDate(
                'last_sent',
                '<>',
                DATE('Y-m-d')
            )->get();

            foreach ($forQueue as $reminder) {
                Mail::to($reminder->user)->queue(
                    new UsersResolutionsReminderMarkdown($reminder)
                );
                $reminder->last_sent = Carbon::now()->format('Y-m-d');
                $reminder->save();
            }

            //if that result (data part) is equal to now() date part send it now - add to mail queue
            //and update last sent (maybe even set last_sent column inside resolutions)
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
