<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Resolution;
use Faker\Generator as Faker;

$factory->define(Resolution::class, function (Faker $faker) {
    $unixTimeMax = '1767139200';//12/31/2025 @ 12:00am (UTC)
    $startTime = $faker->dateTimeBetween('now', $unixTimeMax);
    $untilTime = $faker->dateTimeBetween($startTime, $unixTimeMax);
    $content = "[{\"type\":\"paragraph\",\"children\":[{\"text\":\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a \"},{\"text\":\"galley\",\"bold\":true},{\"text\":\" of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining \"},{\"text\":\"essentially\",\"underline\":true,\"bold\":true},{\"text\":\" unchanged. It was popularised in the \"},{\"text\":\"1960s\",\"code\":true},{\"text\":\" with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like \"},{\"text\":\"Aldus PageMaker\",\"underline\":true},{\"text\":\" including versions of \"},{\"text\":\"Lorem Ipsum\",\"italic\":true},{\"text\":\".\"}]}]";
    $random = 'QWer&c68KLpAwq"34522dfKDLOP';
    $hashed = password_hash($random, PASSWORD_BCRYPT);
    $salt = substr($hashed, 0, 255);
    return [
        'title' => $faker->name,
        'content' => $content,
        'reminder' => random_int(0, 99),
        'start' => $startTime,
        'until' => $untilTime,
        'status' => (bool)random_int(0, 1),
        'published'=> (bool)random_int(0, 1),
        'salt' => $salt,
        'user_id' => 1
    ];
});
