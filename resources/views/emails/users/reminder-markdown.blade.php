@component('vendor.mail.text.message')
# Reminder

Hi {{ $resolution->user->name }}

This is reminder for "{{$resolution->title}}" <br>
You will be emailed every {{ $resolution->reminder }} days!

Please keep going with good work!

If you need some help you should check this detailed step by step guide <a href="https://www.success.com/beyond-resolutions-the-complete-guide-to-achieving-your-new-year-goals" >how to achieve goals</a> . <br>
If you are lack of motivation you should see what <a href="https://www.ted.com/talks?topics%5B%5D=motivation">TED Recommends</a> .

Your success is our success!<br>
<a href="https://www.resolutions.page" >{{ config('app.name') }}</a>
@endcomponent
