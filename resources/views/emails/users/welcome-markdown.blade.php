@component('vendor.mail.text.message')
# Welcome to the Resolutions page

## Hi {{ $user->name }}
### Thank you for joining us!

We are here to give you support in the process of creating resolutions.
You can also rely on us while you track your private or public efforts and achievements.

We hope that you will be satisfied with our service.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
