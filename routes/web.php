<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

/**
 * mail template preview
 */
Route::get('mailable', function () {
    $resolution = App\Resolution::find(1);
    return new App\Mail\ResolutionCreatedMarkdown($resolution);
});

/**
 * mail template preview - welcome
 */
Route::get('welcome', function () {
    $user = App\User::find(1);
    return new App\Mail\UserWelcomeMarkdown($user);
});
